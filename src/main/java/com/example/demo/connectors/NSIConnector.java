package com.example.demo.connectors;

import com.example.demo.datatypes.AZS;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * A static class, consisting of methods,
 * responsible for connecting to NSI.
 */
public class NSIConnector {
    private static final String ALL_AZS_URL = "";
    private final static String ONE_AZS_URL = "";


    //The HTTPClient for making all the requests to the target service
    private static CloseableHttpClient client = HttpClients.createDefault();

    /**Return a list of all AZS objects received from NSI
     * @return a list of AZS objects
     * @throws IOException when an http call fails
     */
    public static List<AZS> requestAllAZSs() throws IOException{
        HttpGet get = new HttpGet(ALL_AZS_URL);
        try(CloseableHttpResponse response = client.execute(get)) {
            StatusLine line = response.getStatusLine();
            if (line.getStatusCode() > 199 && line.getStatusCode() < 99 ){
                throwConnectionException(line, 1);
            }

            List<AZS> list = AZS.createFromEntityList(response.getEntity());
//            return list;
        }catch (IOException e){
            System.out.println(e);
            throw e;
        }

        // return a dummy object
        return Arrays.asList(
            new AZS("1", "1", "4670025051071", "1", "1", "1"),
            new AZS("1", "1", "4670025051354", "1", "1", "1"),
            new AZS("1", "1", "4670025051064", "1", "1", "1"),
            new AZS("1", "1", "4670025051033", "1", "1", "1")
        );
    }

    /**Returns a single AZS object, the gln property
     * of which corresponds to the gln parameter
     * @param gln a GLN code of the required AZS
     * @return an AZS object
     * @throws IOException when an http call fails
     */
    public static AZS requestAZS(String gln) throws IOException{
       HttpGet get = new HttpGet(ONE_AZS_URL + addGlnQuery(gln));
        try(CloseableHttpResponse response = client.execute(get)) {
            StatusLine line = response.getStatusLine();
            if (line.getStatusCode() > 199 && line.getStatusCode() < 99 ){
                throwConnectionException(line, 1);
            }

            AZS azs = AZS.createFromEntity(response.getEntity());
//            return azs;
        }catch (IOException e){
            System.out.println(e);
            throw e;
        }

        // return a dummy object
        return new AZS("1", "1", gln, "1", "1", "1");
    }

    private static String addGlnQuery(String gln) {
        return null;
    }

    private static String getGLNQueryString(String gln) {
        return null;
    }

    public static void connect(String url) throws IOException{
        // connect to the given url, check for it's availability and throw an IOException if it does not work or works
        // incorrectly or is not the proper service.


        HttpGet get = new HttpGet(url);
        //get.
        try (CloseableHttpResponse response = client.execute(get)){
            StatusLine line = response.getStatusLine();
            if (line.getStatusCode() > 399 ){
                throwConnectionException(line, 0);
            }

            HttpEntity entity = response.getEntity();
            BufferedReader stream = new BufferedReader(new InputStreamReader(entity.getContent()));
            List<String> list = stream.lines().collect(Collectors.toList());
            list.forEach(System.out::println);


            if (!compareToDummy(list))
                throw new IOException("The NSI service returned unexpected output!");

        }catch (IOException e){
            throw e;
        }
    }

    private static boolean compareToDummy(List<String> list) {
        //Compare the input to the predefined dummy input
        return true;
    }

    /**
     * Get a generic connection exception
     * @param line A StatusLine of the failed request
     * @param type If 0, make a connection exception, else
     * @return an IOException
     */
    private static void throwConnectionException(StatusLine line, int type) throws IOException{
        throw new IOException(
                (type != 0 ? "NSI error: "
                :"Incorrect target, or target error: ") + "status code - " +
                line.getStatusCode() +", message - " + line.getReasonPhrase());
    }
}
