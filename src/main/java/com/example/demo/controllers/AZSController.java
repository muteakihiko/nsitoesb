package com.example.demo.controllers;

import com.example.demo.connectors.NSIConnector;
import com.example.demo.datatypes.AZS;
import org.springframework.boot.SpringApplication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.List;


/**
 * REST controller class, responsible for
 * handling incoming requests from ESB
 * concerning AZS objects acquired from NSI.
 */
@RestController
@Validated
public class AZSController {
    /**Returns all AZS objects from NSI
     *
     * @return a List of AZS objects
     */
    @RequestMapping(value = "/azs/list", method = RequestMethod.GET, produces = "application/JSON")
    List<AZS> getList(HttpServletRequest request) throws IOException {
        try {

            return NSIConnector.requestAllAZSs();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**Returns an AZS object for a given gln from NSI
     *
     * @param GLN - a GLN number that we want to get the info of
     * @return an AZS object
     */
    @RequestMapping(value = "/azs", method = RequestMethod.GET, produces = "application/JSON")
    AZS getAZS
    (@Valid @Pattern(regexp = "\\d{13}") @RequestParam(value = "gln") String GLN, HttpServletRequest request) throws IOException {
        try {
            System.out.println(request.getQueryString());
            System.out.println(request.getRequestURL().toString());
            return NSIConnector.requestAZS(GLN);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RequestMapping(value = "/connect", method = RequestMethod.GET, produces = "application/JSON")
    String connect (@RequestParam(value = "url", required = false) String url) throws IOException{
        try{
            NSIConnector.connect("http://www.google.com/search?q=mkyong");

            return "ok";
        }catch (IOException e){
            e.printStackTrace();
            throw e;
        }
    }
}
