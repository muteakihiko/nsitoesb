package com.example.demo.datatypes;

import org.apache.http.HttpEntity;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

/**
 * Class, representing AZS info from NSI.
 */
@Validated
public class AZS {
    @NotNull
    @Size(min = 1, max = 16)
    private final String Code_OU_ASKU;
    @NotNull
    @Size(min = 1, max = 150)
    private final String Name_OU_ASKU;
    @NotNull
    @Pattern(regexp = "\\d{13}")
    private final String GLN;
    @NotNull
    @Size(min = 1, max = 150)
    private final String Region_OU_ASKU;
    @NotNull
    @Pattern(regexp = "(Мобильная карта)|(НАМОС)")
    private final String ASU_AZS;
    @NotNull
    @Pattern(regexp = "\\d{10}")
    private final String Code_ASU_AZS;


    public AZS(String code_ou_asku, String name_ou_asku, String gln, String region_ou_asku, String asu_azs, String code_asu_azs) {
        Code_OU_ASKU = code_ou_asku;
        Name_OU_ASKU = name_ou_asku;
        GLN = gln;
        Region_OU_ASKU = region_ou_asku;
        ASU_AZS = asu_azs;
        Code_ASU_AZS = code_asu_azs;
    }

    public String getCode_OU_ASKU() {
        return Code_OU_ASKU;
    }

    public String getName_OU_ASKU() {
        return Name_OU_ASKU;
    }

    public String getGLN() {
        return GLN;
    }

    public String getRegion_OU_ASKU() {
        return Region_OU_ASKU;
    }

    public String getASU_AZS() {
        return ASU_AZS;
    }

    public String getCode_ASU_AZS() {
        return Code_ASU_AZS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AZS azs = (AZS) o;
        return Objects.equals(Code_OU_ASKU, azs.Code_OU_ASKU) &&
                Objects.equals(Name_OU_ASKU, azs.Name_OU_ASKU) &&
                Objects.equals(GLN, azs.GLN) &&
                Objects.equals(Region_OU_ASKU, azs.Region_OU_ASKU) &&
                Objects.equals(ASU_AZS, azs.ASU_AZS) &&
                Objects.equals(Code_ASU_AZS, azs.Code_ASU_AZS);
    }

    @Override
    public int hashCode() {

        return Objects.hash(Code_OU_ASKU, Name_OU_ASKU, GLN, Region_OU_ASKU, ASU_AZS, Code_ASU_AZS);
    }

    public static AZS createFromEntity(HttpEntity entity) {
        return null;
    }

    public static List<AZS> createFromEntityList(HttpEntity entity) {
        return null;
    }
}
