package com.example.demo;

import com.example.demo.datatypes.AZS;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

public class AZSTests {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void AZSValidateOnEmpty1(){
        String testVal = "";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS(testVal, "1",
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert  (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNull_1(){
        String testVal = null;
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS(testVal, "1",
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnTooLong1(){
        String testVal = "1111111111111111111111111111111111111111111111111111111111111";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS(testVal, "1",
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnCorrect1(){
        String testVal = "1";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS(testVal, "1",
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() > 0);
    }

    @Test
    public void AZSValidateOnEmpty2(){
        String testVal = "";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS( "1", testVal,
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNull2(){
        String testVal = null;
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", testVal,
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnTooLong2(){
        String testVal = getLongString();
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS( "1", testVal,
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert(validate.size() < 1);
    }


    @Test
    public void AZSValidateOnCorrect2(){
        String testVal = "1";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS( "1", testVal,
                        "0000000000000", "1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() > 0);
    }

    @Test
    public void AZSValidateOnEmpty3(){
        String testVal = "";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        testVal,"1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNull3(){
        String testVal = null;
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        testVal,"1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnTooLong3(){
        String testVal = "1111111111111111111111111111111111111111111111111111111111111";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        testVal,"1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert(validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNotDigit3(){
        String testVal = "1111111111111111111111111111111111111111111111111111111111111";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        testVal,"1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);

    }

    @Test
    public void AZSValidateOnCorrect3(){
        String testVal = "4670025051064";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        testVal,"1",
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() > 0);
    }

    @Test
    public void AZSValidateOnEmpty4(){
        String testVal = "";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", testVal,
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNull4(){
        String testVal = null;
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", testVal,
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnTooLong4(){
        String testVal = getLongString();
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", testVal,
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnCorrect4(){
        String testVal = "1";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", testVal,
                        "Мобильная карта", "0000000000"));
        System.out.println(validate);
        assert (validate.size() > 0);
    }

    @Test
    public void AZSValidateOnIncorrect5(){
        String testVal = "Мобильная";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        testVal, "0000000000"));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnCorrect5(){
        String testVal = "Мобильная карта";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", testVal,
                        testVal, "0000000000"));
        System.out.println(validate);
        assert  (validate.size() > 0);
    }

    @Test
    public void AZSValidateOnEmpty6(){
        String testVal = "";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        "Мобильная карта", testVal));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNull6(){
        String testVal = null;
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        "Мобильная карта", testVal));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnTooLong6(){
        String testVal = "1111111111111111111111111111111111111111111111111111111111111";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        "Мобильная карта", testVal));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnNotDigit6(){
        String testVal = "not a digit";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        "Мобильная карта", testVal));
        System.out.println(validate);
        assert (validate.size() < 1);
    }

    @Test
    public void AZSValidateOnCorrect6(){
        String testVal = "1111111111";
        Set<ConstraintViolation<AZS>> validate = validator.validate(
                new AZS("1", "1",
                        "0000000000000", "1",
                        "Мобильная карта", testVal));
        System.out.println(validate);
        assert (validate.size() > 1);
    }

    //Convenience method for getting a long string
    private String getLongString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 170; i++) {
            builder.append(i);
        }
        return builder.toString();
    }
}
